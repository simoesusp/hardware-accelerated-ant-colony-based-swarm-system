# Hardware-Accelerated-Ant-Colony-Based-Swarm-System

## Documentacao:
- https://docs.google.com/document/d/1qk4VQJ4Zoe5vtX5szoPPX5S1XCqfWsiE1M2cZvL6-f8/edit?usp=sharing

- https://drive.google.com/file/d/1nRhZb8DdkaonCkEqTryRfoKHZi_3e_jW/view?usp=sharing

The proposal of this work involves a simulation of an ant colony swarm that was applied to a problem of search and rescue of objects of interest.

An evolutionary algorithm was implemented to optimize the ant swarm parameters in order to improve performance on the colony goal.

Hardware acceleration in CUDA and OpenGL provides the system with the ability to simulate a large number of agents simultaneously, in order to study the relevance of swarm size in the performance of a task objective.


Step 1: clone this repository with `git clone https://gitlab.com/simoesusp/hardware-accelerated-ant-colony-based-swarm-system --recursive`

Step 2: Download the following dependencies

GLFW (window calls) - GLM (math calls): https://www.glfw.org/download `sudo apt-get install libglfw3-dev libglm-dev`

No ARK linux => paru -S glfw-x11 glm

Step 3: change the current directory with cd Hardware-Accelerated-Ant-Colony-Based-Swarm-System

Step 4: run `make`

Step 5: run `make run`

OpenGL and CUDA references used:

https://github.com/JoeyDeVries/LearnOpenGL

https://github.com/VictorGordan/opengl-tutorials

https://www.khronos.org/opengl/wiki/Pixel_Buffer_Object

http://www.songho.ca/opengl/gl_pbo.html
